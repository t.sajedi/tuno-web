var blogID = "";
$(function() {
  moment.loadPersian(
    {
      dialect: 'persian-modern',
      usePersianDigits: true
    },
  );
  blogID = location.pathname.split("/").pop();
  getBlogDetail();
});
function setCarouselGenres(element) {
  if ($(window).width() > 1200) {
      $(element).owlCarousel({
          items: 8,
          margin: 10,
          dots: false,
          loop: false,
          autoplay: false,
          nav: false,
          autoplayHoverPause: true,
          rtl: true,
          stagePadding: 55,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  } else if ($(window).width() < 1200 && $(window).width() > 900) {
      $(element).owlCarousel({
          items: 4,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: false,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  } else if ($(window).width() < 900) {
      $(element).owlCarousel({
          items: 2,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: false,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  }
}
function getBlogDetail() {
  if (!blogID) {
    location.replace("/");
  }
  $.ajax({
    type: "GET",
    url: '/api/actor/load/key/'+blogID,
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
          $(".js-video-comments").append(
            `
            <div class="ab-list-title justify-content-start">
              <img src="/api${data.actor.profile}" alt="${data.actor.name}">
              <span class="title mr-5">
              ${data.actor.name}
              </span>
            </div>
            <div class="ab-list-body mb-4">
              ${data.actor.description}
            </div>
            `
          );
          getActorMovies();
    }
  });
}

function getActorMovies() {
  if (!blogID) {
    location.replace("/");
  }
  let body = {actor: blogID};
  $.ajax({
    type: "POST",
    url: "/api/video/search/advanced",
    data: body,
    success: function (data) {
      if(data.videos.length) {
        for (let i = 0; i < data.videos.length; i++) {
          $("#genreActor").append(`
            <div class="item">
            <a class="slide-one" href="/movie/${data.videos[i].key}">
              <div class="slide-image">
                  <img src="/api/media/${data.videos[i].mobile_banners[0]}" loading="lazy" alt="${data.videos[i].name}">
                </div>
                <div class="slide-content">
                  <div class="d-flex justify-content-center">
                    <h2>${data.videos[i].name}</h2>
                  </div>
                </div>
              </a>
            </div>
          `);
        }
        setCarouselGenres("#genreActor");
      }
    },
    error: function (xhr, status, error) {
    }
  });
}