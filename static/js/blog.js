function setCarouselForBanner(element) {
  $(element).owlCarousel({
      autoplay: true,
      autoplayHoverPause: true,
      rtl: false,
      autoplayTimeout: 8000,
      autoplaySpeed: 1500,
      stagePadding: 0,
      items: 1,
      loop:true,
      margin:0,
      singleItem:true,
      nav:true,
      navText: [
          "<img src='../static/img/index/left_double.svg' alt='left icon'>",
          "<img src='../static/img/index/right_double.svg' alt='right icon'>"
      ],
      dots:true,
      transitionStyle : "fadeOut",
      animateIn: "fadeIn",
      animateOut: "fadeOut",
      lazyLoad : true,
      mouseDrag: false,
      touchDrag: false,
  });
}

$(function() {
  moment.loadPersian(
    {
      dialect: 'persian-modern',
      usePersianDigits: true
    }
  );
  getBanners();
  getCategory();
});

function getBanners() {
  $.ajax({
    type: "GET",
    url: "/api/banner/list?blog=true",
    success: function (data) {
      initPagination('');
      if(!data.banners) {
        $("#first-slider").addClass("first-mt-5");
        return;
      }
      for(let i = 0; i < data.banners.length; i++) {
        $("#blog-banner").append(
          `
            <div class="item">
              <div class="banner-content photo-space">
                <div class="slider-wrap px-0">
                  <picture>
                    <source
                        media="(max-width: 768px)"
                        srcset="${data.banners[i].mobile_image}"
                        height="425px">
                    <source
                        media="(min-width: 768px)"
                        srcset="${data.banners[i].descktop_image}"
                        height="800px">
                    <img src="${data.banners[i].descktop_image}" 
                    alt="${data.banners[i].title}" height="800px">
                  </picture>
                  <div class="banner-shadow"></div>
                  <div class="banner-description">
                    <h2>${data.banners[i].title}</h2>
                    <span class="tags">
                      <span>${data.banners[i].description}</span>
                    </span>
                    <div class="d-flex justify-content-end mt-2 js-banner-btns-${data.banners[i].id}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `
        );
        if(data.banners[i].link) {
          $(".js-banner-btns-"+data.banners[i].id).append(
            `
              <a href="${data.banners[i].link}" class="watch btn-primary">
                مطالب بیشتر
              </a>
            `
          );
        }
      }
      setCarouselForBanner("#blog-banner");
    },
    error: function (xhr, status, error) {
      initPagination('');
    }
  });
}

function initPagination(catID) {
  $(".cat-title").each(function() {
    $(".cat-title").removeClass("border-bottom");
  });
  $(".cat-title-"+catID).addClass("border-bottom");
  let url = '/api/blog/list' + (catID ? ('?category='+ catID) : '');
  $('#pagination-container').pagination({
    dataSource: url,
    locator: 'bloges',
    pageSize: 36,
    showLastOnEllipsisShow: false,
    pageRange: 5,
    afterPageOnClick: function(e) {
    },
    afterPreviousOnClick: function(e) {
    },
    afterNextOnClick: function(e) {
    },
    afterPaging: function(e) {
  
      const pervDisableImg = `<img src = '../static/img/common/next.svg' width="35px" height="35px" alt="next icon">`;
      const nextDisableImg = `<img src = '../static/img/common/back.svg' width="35px" height="35px" alt="back icon">`;
  
      const pervImg = `<img src = '../static/img/common/next.svg' width="35px" height="35px" alt="next disabled icon">`;
      const nextImg = `<img src = '../static/img/common/back.svg' width="35px" height="35px" alt="back disabled icon">`;

      const emptyImg = `<img src = '../static/img/common/empty-circle.svg' alt="empty circle icon">`;
      const fullImg = `<img src = '../static/img/common/full-circle.svg' alt="full circle icon">`;
  
      $('.paginationjs-pages .paginationjs-prev.disabled a').html(pervDisableImg);
      $('.paginationjs-pages .paginationjs-next.disabled a').html(nextDisableImg);
  
      $('.paginationjs-pages .J-paginationjs-previous a').html(pervImg);
      $('.paginationjs-pages .J-paginationjs-next a').html(nextImg);

      $('.paginationjs-pages .J-paginationjs-page a').html(emptyImg);
      $('.paginationjs-pages .J-paginationjs-page.active a').html(fullImg);
    },
    callback: function(data, pagination) {
        if(data.length == 0) {
          $(".lists-body").html("<p>ایتم وجود ندارد</p>");
          $("#pagination-container").css("display", "none");
          return;
        }
        var html = template(data, pagination.pageSize, pagination.pageNumber);
        $(".lists-body").html(html);
        if(pagination.totalNumber < 36) {
          $("#pagination-container").css("display", "none");
        } else {
          $("#pagination-container").css("display", "flex");
        }
    }
  });
}

function template(data, pageSize, pageNumber) {
  var html = '';
  $.each(data, function(index, list) {
    html += `
      <div class="col-xl-3 col-md-3 col-6 mb-4">
        <div class="item">
          <a class="slide-one" href="/blogs-detail/${list.key}">
            <div class="slide-image">
              <img src="/api${list.image}" class="blog-img" loading="lazy" alt="${list.title}">
            </div>
            <div class="slide-content">
              <div class="d-flex justify-content-between">
                <h2> ${list.title} </h2>
                <div class="blog-view ${list.view == 0 ? 'd-none' : ''}"><img src="/static/img/common/eye.svg" class="px-1" alt="eye" loading="lazy">${convertToFaDigit(list.view)}</div>
              </div>
              <div class="d-flex justify-content-start">
                <p class="blog-date"> ${moment(list.created_at).format("ddd jD jMMMM jYYYY - HH:mm")} </h2>
              </div>
              <div class="d-flex justify-content-start">
                <p> ${list.content.slice(0, 120)} ... </p>
              </div>
            </div>
          </a>
        </div>
      </div>
    `;
  });
  return html;
}

function getCategory() {
  $.ajax({
    type: "GET",
    url: "/api/category/list?blog=true",
    success: function (data) {
      for(let i = 0; i < data.categoryes.length; i++) {
        $(".cat-lists").append(
          `
            <div class="col-xl-2 col-md-3 col-6 mb-4">
              <div class="item">
                <a class="slide-one" role="button" onclick="initPagination('${data.categoryes[i].id}')">
                  <div class="slide-image cat-blog d-flex">
                    <img src="/api${data.categoryes[i].profile}" loading="lazy" alt="${data.categoryes[i].name}">
                    <div class="d-flex justify-content-center align-items-center px-2">
                      <h4 class="cat-title cat-title-${data.categoryes[i].id}"> ${data.categoryes[i].name} </h4>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          `
        );
      }
    }
  });
}