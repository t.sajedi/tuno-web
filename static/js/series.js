// type: 1 => serial, type: 2 => film
$(function() {
  initPagination();
});
function initPagination() {
  $('#pagination-container').pagination({
    dataSource: '/api/category/data/serial/list',
    alias: {
      pageNumber: 'page',
      pageSize: 'limit'
    },
    locator: 'dataes',
    pageSize: 36,
    showLastOnEllipsisShow: false,
    pageRange: 5,
    afterPageOnClick: function(e) {
    },
    afterPreviousOnClick: function(e) {
    },
    afterNextOnClick: function(e) {
    },
    afterPaging: function(e) {
  
      const pervDisableImg = `<img src = '../static/img/common/next.svg' width="35px" height="35px" alt="next icon">`;
      const nextDisableImg = `<img src = '../static/img/common/back.svg' width="35px" height="35px" alt="back icon">`;
  
      const pervImg = `<img src = '../static/img/common/next.svg' width="35px" height="35px" alt="next disabled icon">`;
      const nextImg = `<img src = '../static/img/common/back.svg' width="35px" height="35px" alt="back disabled icon">`;

      const emptyImg = `<img src = '../static/img/common/empty-circle.svg' alt="empty circle icon">`;
      const fullImg = `<img src = '../static/img/common/full-circle.svg' alt="full circle icon">`;
  
      $('.paginationjs-pages .paginationjs-prev.disabled a').html(pervDisableImg);
      $('.paginationjs-pages .paginationjs-next.disabled a').html(nextDisableImg);
  
      $('.paginationjs-pages .J-paginationjs-previous a').html(pervImg);
      $('.paginationjs-pages .J-paginationjs-next a').html(nextImg);

      $('.paginationjs-pages .J-paginationjs-page a').html(emptyImg);
      $('.paginationjs-pages .J-paginationjs-page.active a').html(fullImg);
      scrollToUp(0, 'slow');
    },
    callback: function(data, pagination) {
        if(data.length == 0) {
          $(".lists-body").html("<p>ایتم وجود ندارد</p>");
          $("#pagination-container").css("display", "none");
          return;
        }
        var html = template(data, pagination.pageSize, pagination.pageNumber);
        $(".lists-body").html(html);
        if(pagination.totalNumber < 36) {
          $("#pagination-container").css("display", "none");
        } else {
          $("#pagination-container").css("display", "flex");
        }
    }
  });
}

function template(data, pageSize, pageNumber) {
  var html = '';
  $.each(data, function(index, list) {
    // let video
    html += `
      <div class="col-xl-2 col-md-3 col-6 mb-4">
        <div class="item">
          <a class="slide-one" href="/movie" onclick="setSerial('${list.season[0].videos[0]}')">
            <div class="slide-image">
              <img src="/api/media/${list.profile}" loading="lazy" alt="${list.name}">
            </div>
            <div class="slide-content">
              <div class="d-flex justify-content-start">
                <h2> ${list.name} </h2>
              </div>
            </div>
          </a>
        </div>
      </div>
    `;
  });
  return html;
}