var blogID = "";
$(function() {
  moment.loadPersian(
    {
      dialect: 'persian-modern',
      usePersianDigits: true
    },
  );
  blogID = location.pathname.split("/").pop();
  getBlogDetail();
});

function getBlogDetail() {
  if (!blogID) {
    location.replace("/blogs");
  }
  $.ajax({
    type: "GET",
    url: '/api/blog/load/key/'+ blogID,
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
      if(!data.blog.release) {
        location.href = '/blogs';
        return;
      }
          $(".js-video-comments").append(
            `
            <div class="comments-list-title justify-content-between">
              <img src="${data.blog.image}" alt="${data.blog.title}">
              <span class="title mr-5">
              ${data.blog.title} <span style="font-size: 12px;color: #868686;"> | ${moment(data.blog.created_at).format("ddd jD jMMMM")}</span> <span style="font-size: 12px; color: #868686;">${data.blog.view ? ('(بازدید: ' + convertToFaDigit(data.blog.view) + ")") : ''}</span>
              </span>
            </div>
            <div class="comments-list-body mb-4">
              ${data.blog.content}
            </div>
            <hr>
            `
          );
    },
    error: function (xhr, status, error) {
      // msg
      location.replace("/blogs");
    }
  });
}