var validator = "";
function login() {
  var $form = $("#login-form");
  $.validator.addMethod(
    "email_phone",
    function (value) {
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      const ePattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/, 'i');
      if (pattern.test(convertToEnDigit(value))) {
        return true;
      }
      else if (ePattern.test(convertToEnDigit(value))) {
        return true;
      }
      else {
        return false;
      }
    },
    "شماره موبایل نامعتبر است"
  );
  validator = $form.validate({
    ignore: ".ignore",
    rules: {
      input: {
        required: true,
        email_phone: true,
        maxlength: 100
      },
      password: {
        required: true,
        minlength: 8,
        maxlength: 50,
      }
    },
    messages: {
      input: {
          'required': 'شماره موبایل یا ایمیل را وارد نمایید',
          'email_phone': 'شماره موبایل یا ایمیل نامعتبر است',
          'maxlength': 'شماره موبایل طولانی است'
      },
      password: {
        required: "رمز عبور را وارد نمایید",
        minlength: "رمز عبور باید بیش از ۸ حرف یا عدد باشد",
        maxlength: "رمز عبور وارد شده طولانی است",
      }
    },
    submitHandler: function () {
      let body = {
        input: convertToEnDigit($("#login-input").val()),
        password: convertToEnDigit($("#login-password").val())
      }
      $.ajax({
        type: "POST",
        url: "/api/user/login",
        data: body,
        beforeSend: function () {
          showLoader();
        },
        success: function (data) {
          hideLoader();
          localStorage.setItem("current-token", data.token);
          window.location.replace("/");
        },
        error: function (xhr, status, error) {
          hideLoader();
          if(xhr && xhr.responseJSON) {
            if(xhr.responseJSON["code"] == 4002) {
              $(".js-error").html("اطلاعات وارد شده صحیح نیست.");
            }
            else if(xhr.responseJSON["code"] == 400032) {
              $(".js-error").html("رمز عبور اشتباه است.");
            }
            else if(xhr.responseJSON["code"] == 400034 && xhr.responseJSON["error"] == "please try after 10 min") {
              $(".js-error").html("کد ارسال شده است،لطفا بعد از چند دقیقه دوباره امتحان کنید.");
            }
            else if(xhr.responseJSON["code"] == 400033) {
              $(".js-error").html("شماره موبایل یا ایمیل مورد نظر تایید نشده است.");
            }
            else if(xhr.responseJSON["code"] == 400030 && xhr.responseJSON["debug_msg"] == "password otp count is greather than 3") {
              $(".js-error").html("تعداد درخواست رمزیکبار مصرف بیش از حد مجاز می باشد،لطفا بعدا مجدد تلاش نمایید.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if(xhr.responseJSON["code"] == 400030) {
              $(".js-error").html("زمان استفاده از رمز یکبار مصرف به پایان رسیده است.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if (xhr.responseJSON["code"] == 4004 && xhr.responseJSON["error"] == "not found") {
              $(".js-error").html("کاربر یافت نشد.");
            }
            else {
              $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
            }
          }
          else {
            $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
          }
          setTimeout(function() {
            $(".js-error").html("");
          }, 7000);
        }
      });
      return false;
    },
  });
}

$(document).ready(function() {
  login();
});

$("#login-form").on("submit", function() {
  $(".ignore").each(function() {
    $(this).removeClass("ignore");
  });
});
