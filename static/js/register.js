var validator = "";
function registerByMobile() {
  var $form = $("#register-form");
  $.validator.addMethod(
    "phone",
    function (value) {
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      return pattern.test(convertToEnDigit(value));
    },
    "شماره موبایل نامعتبر است"
  );
  validator = $form.validate({
    ignore: ".ignore",
    rules: {
      mobile: {
        required: true,
        phone: true,
        maxlength: 255
      }
    },
    messages: {
      mobile: {
          'required': 'شماره موبایل را وارد نمایید',
          'phone': 'شماره موبایل نامعتبر است',
          'maxlength': 'شماره موبایل طولانی است'
      }
    },
    submitHandler: function () {
      $.ajax({
        type: "GET",
        url: `/api/user/signup/mobile?mobile=${convertToEnDigit("0"+$("#signupMobile").val())}`,
        beforeSend: function () {
          showLoader();
        },
        success: function () {
          hideLoader();
          window.location.replace(`check-otp.html?input=${convertToEnDigit("0"+$("#signupMobile").val())}&page=register`);
        },
        error: function (xhr, status, error) {
          hideLoader();
          if(xhr.responseJSON && xhr.responseJSON["code"] == 40012) {
            $(".js-error").html("کاربر با شماره موبایل مورد نظر قبلا وجود دارد");
          }
          else if(xhr.responseJSON && xhr.responseJSON["code"] == 4002) {
            $(".js-error").html("اطلاعات وارد شده صحیح نمیباشد");
          }
          else {
            $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
          }
          setTimeout(function() {
            $(".js-error").html("");
          }, 7000)
        }
      });
      return false;
    },
  });

}

$(document).ready(function() {
  registerByMobile();
});

$("#register-form").on("submit", function() {
  $(".ignore").each(function() {
    $(this).removeClass("ignore");
  });
});

// register2
var validator2 = "";
function register2() {
  var $form = $("#register2-form");
  $.validator.addMethod(
    "phone",
    function (value) {
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      return pattern.test(convertToEnDigit(value));
    },
    "شماره موبایل نامعتبر است"
  );
  $.validator.addMethod(
      "regex",
      function(value, element, regexp) {
        var regex = new RegExp(regexp);
          return this.optional(element) || regex.test(value);
      },
      'رمز عبور باید شامل حروف بزرگ و کوچک انگلیسی و عدد باشد'
  );
  validator2 = $form.validate({
    ignore: ".ignore",
    rules: {
      firstName: {
        required: true,
        maxlength: 150,
        minlength: 2
      },
      lastName: {
        required: true,
        maxlength: 150,
        minlength: 2
      },
      password: {
        required: true,
        maxlength: 18,
        minlength: 6,
        regex: '(?=[A-Za-z0-9]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,}).*$'
      },
      confirmPassword: {
        required: true,
        minlength: 6,
        maxlength: 18,
        equalTo: "#valPassword"
      }
    },
    messages: {
      firstName: {
        required: 'نام خود را وارد نمایید',
        maxlength: 'تعداد کاراکتر وارد شده بیش از حد مجاز میباشد',
        minlength: 'تعداد کاراکتر وارد شده حداقل باید ۲ حرف باشد'
      },
      lastName: {
        required: 'نام خانوادگی خود را وارد نمایید',
        maxlength: 'تعداد کاراکتر وارد شده بیش از حد مجاز میباشد',
        minlength: 'تعداد کاراکتر وارد شده حداقل باید ۲ حرف باشد'
      },
      password: {
        required: 'رمز عبور خود را وارد نمایید',
        maxlength: 'رمز عبور حداکثر باید شامل ۱۸ کارکتر باشد',
        minlength: 'رمز عبور حداقل باید ۶ کارکتر باشد'
      },
      confirmPassword: {
        required: 'رمز عبور خود را تکرار کنید',
        minlength: 'رمز عبور حداقل باید ۶ کارکتر باشد',
        maxlength: 'رمز عبور حداکثر باید شامل ۱۸ کارکتر باشد',
        equalTo: 'رمز عبور و تکرار آن مطابقت ندارند'
      }
    },
    submitHandler: function () {
      let body = {
        input: getParameterByName('input'),
        fullname: $("#register-firstname").val() + " " + $("#register-lastname").val(),
        password: $("#valPassword").val()
      };
      $.ajax({
        type: "POST",
        url: '/api/user/save',
        data: body,
        beforeSend: function () {
          showLoader();
        },
        success: function (data) {
          hideLoader();
          localStorage.setItem("current-token", data.token);
          window.location.replace("index.html");
        },
        error: function (xhr, status, error) {
          hideLoader();
          if(xhr.responseJSON && xhr.responseJSON["code"] == 40012) {
            $(".js-error").html("کاربر با شماره موبایل مورد نظر قبلا وجود دارد");
          }
          else if(xhr.responseJSON && xhr.responseJSON["code"] == 4002) {
            $(".js-error").html("اطلاعات وارد شده صحیح نمیباشد");
          }
          else {
            $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
          }
          setTimeout(function() {
            $(".js-error").html("");
          }, 7000)
        }
      });
      return false;
    },
  });

}

$(document).ready(function() {
  register2();
});

$("#register2-form").on("submit", function() {
  $(".ignore").each(function() {
    $(this).removeClass("ignore");
  });
});