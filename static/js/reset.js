var validator = "";
function reset() {
  var $form = $("#reset-form");
  $.validator.addMethod(
    "email_phone",
    function (value) {
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      const ePattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/, 'i');
      if (pattern.test(convertToEnDigit(value))) {
        return true;
      }
      else if (ePattern.test(convertToEnDigit(value))) {
        return true;
      }
      else {
        return false;
      }
    },
    "شماره موبایل یا ایمیل نامعتبر است"
  );
  validator = $form.validate({
    ignore: ".ignore",
    rules: {
      input: {
        required: true,
        email_phone: true,
        maxlength: 100
      }
    },
    messages: {
      input: {
          'required': 'شماره موبایل یا ایمیل را وارد نمایید',
          'email_phone': 'شماره موبایل یا ایمیل نامعتبر است',
          'maxlength': 'شماره موبایل یا ایمیل طولانی است'
      }
    },
    submitHandler: function () {
      let body;
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      const ePattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/, 'i');
      if(pattern.test(convertToEnDigit($("#reset-input").val()))) {
        body = {
          mobile: convertToEnDigit($("#reset-input").val())
        }
      }
      else {
        body = {
          email: convertToEnDigit($("#reset-input").val())
        }
      }
      $.ajax({
        type: "POST",
        url: "/api/user/reset/password",
        data: body,
        beforeSend: function () {
          showLoader();
        },
        success: function () {
          hideLoader();
          window.location.replace(`check-otp.html?input=${convertToEnDigit($("#reset-input").val())}&page=reset`);
        },
        error: function (xhr, status, error) {
          hideLoader();
          if(xhr && xhr.responseJSON) {
            if(xhr.responseJSON["code"] == 4002) {
              $(".js-error").html("اطلاعات وارد شده صحیح نیست.");
            }
            else if(xhr.responseJSON["code"] == 400034 && xhr.responseJSON["error"] == "please try after 10 min") {
              $(".js-error").html("کد ارسال شده است،لطفا بعد از چند دقیقه دوباره امتحان کنید.");
            }
            else if(xhr.responseJSON["code"] == 400033) {
              $(".js-error").html("شماره موبایل یا ایمیل مورد نظر تایید نشده است.");
            }
            else if (xhr.responseJSON["code"] == 4004 && xhr.responseJSON["error"] == "not found") {
              $(".js-error").html("کاربر یافت نشد.");
            }
            else {
              $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
            }
          }
          else {
            $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
          }
          setTimeout(function() {
            $(".js-error").html("");
          }, 7000);
        }
      });
      return false;
    },
  });

}

$(document).ready(function() {
  reset();
  // email or mobile validation
  $('#reset-input').on('blur keypress input paste', function (e) {
    if(justEnglish(e.key) === false) {
      e.preventDefault();
      return false;
    }
  });
});

function justEnglish(str) {
  var p = /^[\u0600-\u06FF\s]+$/;
  if (p.test(str)) {
    return false;
  }
  return true;
}

$("#reset-form").on("submit", function() {
  $(".ignore").each(function() {
    $(this).removeClass("ignore");
  });
});

// set new password
var validator2 = "";
function savePassword() {
  var $form = $("#new-password-form");
  $.validator.addMethod(
    "email_phone",
    function (value) {
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      const ePattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/, 'i');
      if (pattern.test(convertToEnDigit(value))) {
        return true;
      }
      else if (ePattern.test(convertToEnDigit(value))) {
        return true;
      }
      else {
        return false;
      }
    },
    "شماره موبایل یا ایمیل نامعتبر است"
  );
  $.validator.addMethod(
    "regex",
    function(value, element, regexp) {
      var regex = new RegExp(regexp);
        return this.optional(element) || regex.test(value);
    },
    'رمز عبور باید شامل حروف بزرگ و کوچک انگلیسی و عدد باشد'
  );
  validator2 = $form.validate({
    ignore: ".ignore",
    rules: {
      input: {
        required: true,
        email_phone: true,
        maxlength: 100
      },
      password: {
        required: true,
        maxlength: 18,
        minlength: 6,
        regex: '(?=[A-Za-z0-9]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,}).*$'
      },
      confirmPassword: {
        required: true,
        minlength: 6,
        maxlength: 18,
        equalTo: "#valPassword"
      }
    },
    messages: {
      input: {
          'required': 'شماره موبایل یا ایمیل را وارد نمایید',
          'email_phone': 'شماره موبایل یا ایمیل نامعتبر است',
          'maxlength': 'شماره موبایل یا ایمیل طولانی است'
      },
      password: {
        required: 'رمز عبور خود را وارد نمایید',
        maxlength: 'رمز عبور حداکثر باید شامل ۱۸ کارکتر باشد',
        minlength: 'رمز عبور حداقل باید ۶ کارکتر باشد'
      },
      confirmPassword: {
        required: 'رمز عبور خود را تکرار کنید',
        minlength: 'رمز عبور حداقل باید ۶ کارکتر باشد',
        maxlength: 'رمز عبور حداکثر باید شامل ۱۸ کارکتر باشد',
        equalTo: 'رمز عبور و تکرار آن مطابقت ندارند'
      }
    },
    submitHandler: function () {
      let body;
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      const ePattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/, 'i');
      if(pattern.test(convertToEnDigit($("#new-password-input").val()))) {
        body = {
          mobile: convertToEnDigit($("#new-password-input").val())
        }
      }
      else {
        body = {
          email: convertToEnDigit($("#new-password-input").val())
        }
      }
      body["password"] = $("#valPassword").val();
      $.ajax({
        type: "POST",
        url: "/api/user/reset/check",
        data: body,
        beforeSend: function () {
          showLoader();
        },
        success: function () {
          hideLoader();
          window.location.replace('auth.html');
        },
        error: function (xhr, status, error) {
          hideLoader();
          if(xhr && xhr.responseJSON) {
            if(xhr.responseJSON["code"] == 4002) {
              $(".js-error").html("اطلاعات وارد شده صحیح نیست.");
            }
            else if(xhr.responseJSON["code"] == 400034 && xhr.responseJSON["error"] == "please try after 10 min") {
              $(".js-error").html("کد ارسال شده است،لطفا بعد از چند دقیقه دوباره امتحان کنید.");
            }
            else if(xhr.responseJSON["code"] == 400030 && xhr.responseJSON["debug_msg"] == "password otp count is greather than 3") {
              $(".js-error").html("تعداد درخواست رمزیکبار مصرف بیش از حد مجاز می باشد،لطفا بعدا مجدد تلاش نمایید.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if(xhr.responseJSON["code"] == 400030) {
              $(".js-error").html("زمان استفاده از رمز یکبار مصرف به پایان رسیده است.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if(xhr.responseJSON["code"] == 400033) {
              $(".js-error").html("شماره موبایل یا ایمیل مورد نظر تایید نشده است.");
            }
            else if (xhr.responseJSON["code"] == 4004 && xhr.responseJSON["error"] == "not found") {
              $(".js-error").html("کاربر یافت نشد.");
            }
            else {
              $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
            }
          }
          else {
            $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
          }
          setTimeout(function() {
            $(".js-error").html("");
          }, 7000);
        }
      });
      return false;
    },
  });
}

$(document).ready(function() {
  savePassword();
  if(getParameterByName('input') == "") {
    return location.href = "reset.html";
  }
  $("#new-password-input").val(getParameterByName('input'));
});

$("#new-password-form").on("submit", function() {
  $(".ignore").each(function() {
    $(this).removeClass("ignore");
  });
});
