$(function() {
  $(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
      $(".main-header").addClass("active");
    } else {
      $(".main-header").removeClass("active");
    }
  });
  // check Current user
  if(sessionStorage.getItem("current-user") != null) {
    // show user stuff
    setUserTemplate();
  }
  else {
    setDefaultTemplate();
  }
  if(localStorage.getItem("current-token") != null) {
    getCurrentUser();
  }
});

function getCurrentUser() {
  $.ajax({
    type: "GET",
    url: "/api/user/current",
    headers: {
      "Authorization": localStorage.getItem("current-token")
    },
    success: function (data) {
      sessionStorage.setItem("current-user", JSON.stringify(data.user));
      setUserTemplate();
    },
    error: function (xhr, status, error) {
      sessionStorage.removeItem("current-user", null);
      setDefaultTemplate();
    }
  });
}

function setUserTemplate() {
  $(".js-user").html(
    `
    <div class="user-name ml-2" id="search-item">
    <div class="d-flex d-sm-none text-light ps-1 h5 m-0">
      <svg width="21" height="21" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
        <ellipse cx="16.5" cy="16.5" rx="10.5" ry="10.5" stroke="#CCD2E3" stroke-width="2"/>
        <path d="M30 30L25.5 25.5" stroke="#CCD2E3" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </div>
    <div class="user d-none d-sm-flex ml-3">
      <svg width="21" height="21" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
        <ellipse cx="16.5" cy="16.5" rx="10.5" ry="10.5" stroke="#CCD2E3" stroke-width="2"/>
        <path d="M30 30L25.5 25.5" stroke="#CCD2E3" stroke-width="2" stroke-linecap="round"/>
      </svg>                      
    </div>
  </div>
  <div class="user-name dropdown" tabindex="0">
    <div class="dropdown-label user-image-box">
      <svg width="25" height="25" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.87868 9.21193C5 10.0906 5 11.5048 5 14.3333V25.6666C5 28.495 5 29.9092 5.87868 30.7879C6.75736 31.6666 8.17157 31.6666 11 31.6666H29C31.8284 31.6666 33.2426 31.6666 34.1213 30.7879C35 29.9092 35 28.495 35 25.6666V14.3333C35 11.5048 35 10.0906 34.1213 9.21193C33.2426 8.33325 31.8284 8.33325 29 8.33325H11C8.17157 8.33325 6.75736 8.33325 5.87868 9.21193ZM10.5547 14.1679C10.0952 13.8615 9.4743 13.9857 9.16795 14.4452C8.8616 14.9048 8.98577 15.5256 9.4453 15.832L18.8906 22.1289C19.5624 22.5767 20.4376 22.5767 21.1094 22.1289L30.5547 15.832C31.0142 15.5256 31.1384 14.9048 30.8321 14.4452C30.5257 13.9857 29.9048 13.8615 29.4453 14.1679L20 20.4648L10.5547 14.1679Z" fill="white"/>
      </svg>
    </div>
    <ul class="dropdown-list msg-box">
      <li>
        <div class="d-flex item">
          <img src="../static/img/common/avatar.png" width="50px" height="50px" alt="avatar">
          <div class="text-box">
            <span>زینب چمران</span>
            <div class="d-flex flex-wrap justify-content-between align-items-center text-msg">
              <span>بابا میدونم چیه حوصله ندارم انجامش بدم</span>
              <p class="time">5 : 58 PM</p>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="d-flex item pt-3">
          <img src="../static/img/common/avatar.png" width="50px" height="50px" alt="avatar">
          <div class="text-box">
            <span>زینب چمران</span>
            <div class="d-flex flex-wrap justify-content-between align-items-center text-msg">
              <span>بابا میدونم چیه حوصله ندارم انجامش بدم</span>
              <p class="time">5 : 58 PM</p>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="d-flex item pt-3">
          <img src="../static/img/common/avatar.png" width="50px" height="50px" alt="avatar">
          <div class="text-box">
            <span>زینب چمران</span>
            <div class="d-flex flex-wrap justify-content-between align-items-center text-msg">
              <span>بابا میدونم چیه حوصله ندارم انجامش بدم</span>
              <p class="time">5 : 58 PM</p>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="d-flex item pt-3">
          <img src="../static/img/common/avatar.png" width="50px" height="50px" alt="avatar">
          <div class="text-box">
            <span>زینب چمران</span>
            <div class="d-flex flex-wrap justify-content-between align-items-center text-msg">
              <span>بابا میدونم چیه حوصله ندارم انجامش بدم</span>
              <p class="time">5 : 58 PM</p>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <div class="user-name dropdown me-3" id="user-item" tabindex="0">
    <div class="dropdown-label user-image-box">
      <img src="../static/img/common/avatar.png" width="40px" height="40px" alt="avatar">
    </div>
    <ul class="dropdown-list">
      <li onclick='logout()'>
        <span>
          <svg class="ps-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M17.294 7.29105C17.294 10.2281 14.9391 12.5831 12 12.5831C9.0619 12.5831 6.70602 10.2281 6.70602 7.29105C6.70602 4.35402 9.0619 2 12 2C14.9391 2 17.294 4.35402 17.294 7.29105ZM12 22C7.66237 22 4 21.295 4 18.575C4 15.8539 7.68538 15.1739 12 15.1739C16.3386 15.1739 20 15.8789 20 18.599C20 21.32 16.3146 22 12 22Z" fill="white"/>
          </svg>                          
          تنظیمات حساب
        </span>
      </li>
      <li>
        <span>
          <svg class="ps-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M19.761 12.001C19.761 12.8145 20.429 13.4764 21.25 13.4764C21.664 13.4764 22 13.8093 22 14.2195V16.8958C22 19.159 20.142 21 17.858 21H6.143C3.859 21 2 19.159 2 16.8958V14.2195C2 13.8093 2.336 13.4764 2.75 13.4764C3.572 13.4764 4.24 12.8145 4.24 12.001C4.24 11.2083 3.599 10.6118 2.75 10.6118C2.551 10.6118 2.361 10.5335 2.22 10.3938C2.079 10.2541 2 10.0648 2 9.86866L2.002 7.10514C2.002 4.84201 3.86 3 6.144 3H17.856C20.14 3 21.999 4.84201 21.999 7.10514L22 9.78245C22 9.97864 21.921 10.1689 21.781 10.3076C21.64 10.4473 21.45 10.5256 21.25 10.5256C20.429 10.5256 19.761 11.1875 19.761 12.001ZM14.252 12.648L15.431 11.5105C15.636 11.3143 15.707 11.025 15.618 10.7575C15.53 10.4899 15.3 10.2997 15.022 10.261L13.393 10.0252L12.664 8.56271C12.539 8.31103 12.285 8.15447 12.002 8.15348H12C11.718 8.15348 11.464 8.31004 11.337 8.56172L10.608 10.0252L8.98202 10.2601C8.70102 10.2997 8.47102 10.4899 8.38202 10.7575C8.29402 11.025 8.36502 11.3143 8.56902 11.5105L9.74802 12.648L9.47002 14.2562C9.42202 14.5336 9.53502 14.8091 9.76502 14.9746C9.89502 15.0667 10.046 15.1143 10.199 15.1143C10.316 15.1143 10.434 15.0856 10.542 15.0291L12 14.2701L13.455 15.0271C13.707 15.1609 14.006 15.14 14.235 14.9736C14.466 14.8091 14.579 14.5336 14.531 14.2562L14.252 12.648Z" fill="white"/>
          </svg>
          خرید اشتراک
        </span>
      </li>
      <li>
        <span>
          <svg class="ps-2" width="20" height="20" viewBox="0 0 20 23" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M16.1261 6.20541V7.96875C18.1068 8.587 19.55 10.3801 19.55 12.5217V18.1991C19.55 20.8504 17.3519 23 14.642 23H4.90912C2.19806 23 0 20.8504 0 18.1991V12.5217C0 10.3801 1.44434 8.587 3.42388 7.96875V6.20541C3.43556 2.777 6.27516 0 9.75747 0C13.2865 0 16.1261 2.777 16.1261 6.20541ZM9.78073 2.00024C12.1529 2.00024 14.081 3.88586 14.081 6.20575V7.7211H5.46875V6.18289C5.48044 3.87444 7.40856 2.00024 9.78073 2.00024ZM10.7969 16.6232C10.7969 17.1831 10.3412 17.6288 9.7686 17.6288C9.20769 17.6288 8.75195 17.1831 8.75195 16.6232V14.0862C8.75195 13.5376 9.20769 13.0919 9.7686 13.0919C10.3412 13.0919 10.7969 13.5376 10.7969 14.0862V16.6232Z" fill="white"/>
          </svg>                          
          عوض کردن رمز
        </span>
      </li>
      <li>
        <span>
          <svg class="ps-2" width="23" height="25" viewBox="0 0 23 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M20.8656 9.72467C20.8656 11.1829 21.251 12.0425 22.0992 13.033C22.742 13.7627 22.9474 14.6994 22.9474 15.7156C22.9474 16.7307 22.6139 17.6943 21.9457 18.4767C21.071 19.4145 19.8374 20.0133 18.5784 20.1174C16.754 20.2729 14.9284 20.4039 13.0785 20.4039C11.2275 20.4039 9.40308 20.3255 7.57863 20.1174C6.31848 20.0133 5.08487 19.4145 4.21131 18.4767C3.54315 17.6943 3.2085 16.7307 3.2085 15.7156C3.2085 14.6994 3.41506 13.7627 4.05667 13.033C4.93139 12.0425 5.29144 11.1829 5.29144 9.72467V9.23C5.29144 7.27706 5.77842 6.00004 6.78123 4.74993C8.27218 2.92679 10.6621 1.83337 13.0266 1.83337H13.1305C15.5457 1.83337 18.013 2.97941 19.4785 4.8809C20.4294 6.10529 20.8656 7.32851 20.8656 9.23V9.72467ZM9.68076 22.804C9.68076 22.2193 10.2174 21.9515 10.7136 21.8369C11.294 21.7141 14.831 21.7141 15.4114 21.8369C15.9077 21.9515 16.4443 22.2193 16.4443 22.804C16.4154 23.3606 16.0888 23.8541 15.6376 24.1675C15.0526 24.6236 14.3659 24.9125 13.6482 25.0165C13.2512 25.068 12.8611 25.0692 12.478 25.0165C11.7591 24.9125 11.0725 24.6236 10.4886 24.1664C10.0362 23.8541 9.70961 23.3606 9.68076 22.804Z" fill="white"/>
          </svg>
          اعلانات
        </span>
      </li>
      <li>
        <span>
          <svg class="ps-2" width="20" height="20" viewBox="0 0 17 21" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M5.09141 0H11.5024C14.3183 0 16.5938 1.12346 16.625 3.97935V19.9177C16.625 20.0962 16.5834 20.2747 16.5003 20.4322C16.3652 20.6842 16.1366 20.8732 15.8561 20.9572C15.5859 21.0412 15.2846 20.9992 15.0352 20.8522L8.30211 17.4503L1.55859 20.8522C1.40377 20.9351 1.22609 20.9887 1.04945 20.9887C0.467578 20.9887 0 20.5057 0 19.9177V3.97935C0 1.12346 2.28594 0 5.09141 0ZM4.38566 8.00012H12.2098C12.6566 8.00012 13.0203 7.63159 13.0203 7.17065C13.0203 6.70867 12.6566 6.34119 12.2098 6.34119H4.38566C3.93887 6.34119 3.5752 6.70867 3.5752 7.17065C3.5752 7.63159 3.93887 8.00012 4.38566 8.00012Z" fill="white"/>
          </svg>
          فیلم های ذخیره شده
        </span>
      </li>
    </ul>
  </div>
    `
  );
}

function logout() {
  localStorage.removeItem("current-token");
  sessionStorage.removeItem("current-user");
  location.href = "/";
}

function setDefaultTemplate() {
  $(".js-user").html(
    `
      <div class="user-name ml-2" id="search-item">
        <div class="d-flex d-sm-none text-light h5 ml-3">
          <svg width="21" height="21" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
            <ellipse cx="16.5" cy="16.5" rx="10.5" ry="10.5" stroke="#CCD2E3" stroke-width="2"/>
            <path d="M30 30L25.5 25.5" stroke="#CCD2E3" stroke-width="2" stroke-linecap="round"/>
          </svg>                  
        </div>
        <div class="user d-none d-sm-flex ml-3">
          <svg width="21" height="21" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
            <ellipse cx="16.5" cy="16.5" rx="10.5" ry="10.5" stroke="#CCD2E3" stroke-width="2"/>
            <path d="M30 30L25.5 25.5" stroke="#CCD2E3" stroke-width="2" stroke-linecap="round"/>
          </svg>                      
        </div>
      </div>
      <div class="user-name" id="plan-item">
        <div class="d-block d-sm-none text-light h5 ml-3" onclick="location.replace('/plan')">
          خرید اشتراک
        </div>
        <a href="/plan.html" class="btn-default d-none d-sm-flex ml-3">
          خرید اشتراک
        </a>
      </div>
      <div class="user-name ml-2" id="auth-item">
          <div class="d-flex d-lg-none text-light h5 m-0" onclick="location.href = '/auth.html';">
            <img src="../static/img/common/login.svg" width="21px" height="21px" alt="sign in icon">
          </div>
          <!-- <a href="./auth.html" class="btn-primary d-none d-lg-flex">
            <span>ورود / </span><span>ثبت نام</span>
          </a> -->
          <div class="btn-primary d-none d-lg-flex">
            <span onclick="location.href='./auth.html'"> ورود </span>
            <span onclick="location.href='./auth.html'">/</span>
            <span onclick="location.href='./register.html'"> ثبت نام </span>
          </div>
    </div>
    `
  )
}

function scrollToUp(space, duration) {
  $("html, body").animate({ scrollTop: space }, duration);
}

convertToEnDigit = function (str) {
  return str
    .replace(/۰/g, "0")
    .replace(/۱/g, "1")
    .replace(/۲/g, "2")
    .replace(/۳/g, "3")
    .replace(/۴/g, "4")
    .replace(/۵/g, "5")
    .replace(/۶/g, "6")
    .replace(/۷/g, "7")
    .replace(/۸/g, "8")
    .replace(/۹/g, "9")
    .replace(/٠/g, "0")
    .replace(/١/g, "1")
    .replace(/٢/g, "2")
    .replace(/٣/g, "3")
    .replace(/٤/g, "4")
    .replace(/٥/g, "5")
    .replace(/٦/g, "6")
    .replace(/٧/g, "7")
    .replace(/٨/g, "8")
    .replace(/٩/g, "9");
};

convertToFaDigit = function (str) {
  str = str.toString();
  var inputs = ""+str;
  new_val = inputs
  .replace(/0/g, '۰')
  .replace(/1/g, '۱')
  .replace(/2/g, '۲')
  .replace(/3/g, '۳')
  .replace(/4/g, '۴')
  .replace(/5/g, '۵')
  .replace(/6/g, '۶')
  .replace(/7/g, '۷')
  .replace(/8/g, '۸')
  .replace(/9/g, '۹')
  .replace(/٤/g, '۴')
  .replace(/٥/g, '۵')
  .replace(/٦/g, '۶');
  return new_val;
};

function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function showLoader() {
  if ($('[data-remodal-id="loader"]').length) {
    $('[data-remodal-id="loader"]').remodal().open();
  }
}

function hideLoader() {
  setTimeout(function () {
    if ($('[data-remodal-id="loader"]').length) {
      $('[data-remodal-id="loader"]').remodal().close();
    }
  }, 500);
}

function startPreview(videoID, page) {
  if(page == "main-page") {
    getVideoPreview(videoID);
  }
  else {
    $("#questionModal").slideToggle(100);
    $('.js-questions').empty();
    $('.js-questions').append(`
      <div class="col-lg-12 d-flex justify-content-end">
        <span class="cursor-pointer" title="بستن" onclick="closeModal()">
          <img src="../static/img/common/cross.svg" alt="cross">
        </span>
      </div>
      <div class="col-lg-12">
        <video id="quiz-video" class="w-100" src="${videoID}" autoplay playsinline controls></video>
      </div>
    `);
  }
  return;
}

function getVideoPreview(videoID) {
  $.ajax({
    type: "GET",
    url: "/api/video/load/"+videoID,
    success: function (data) {
      if(data.video.preview) {
        $("#questionModal").slideToggle(100);
        $('.js-questions').empty();
        $('.js-questions').append(`
          <div class="col-lg-12 d-flex justify-content-end">
            <span class="cursor-pointer" title="بستن" onclick="closeModal()">
              <img src="../static/img/common/cross.svg" alt="cross">
            </span>
          </div>
          <div class="col-lg-12">
            <video id="quiz-video" class="w-100" src="${data.video.preview}" autoplay playsinline controls></video>
          </div>
        `);
      }
    },
    error: function (xhr, status, error) {
    }
  });
}

function closeModal() {
  $("#questionModal").slideToggle(100);
  document.getElementById("quiz-video").pause();
}

function closeModalVideo() {
  $(".questions-wrapper").empty();
  $("#filmModal").slideToggle(100);
}

function splitter(list, keyName, splitter = '- ') {
  if (!list.length) return;
  if (list.length == 1) {return list[0][keyName]};
  let newList = [];
  for (let key in list) {
    if(list[key][keyName]) {
      newList.push(list[key][keyName]);
    }
  }
  return newList.join(splitter);
}

var ext = (function(file, ty) {
  return ty === -1 ? undefined : file.substring(ty+1); 
});

function setSerial(videoID) {
  localStorage.setItem("fb-serial", videoID);
}

function setCategoryID(catID) {
  localStorage.setItem("fb-category", catID);
}