function setCarouselForBanner(element) {
  $(element).owlCarousel({
      autoplay: true,
      autoplayHoverPause: true,
      rtl: false,
      autoplayTimeout: 8000,
      autoplaySpeed: 1500,
      stagePadding: 0,
      items: 1,
      loop:true,
      margin:0,
      singleItem:true,
      nav:true,
      navText: [
          "<img src='../static/img/index/left_double.svg' alt='left icon'>",
          "<img src='../static/img/index/right_double.svg' alt='right icon'>"
      ],
      dots:true,
      transitionStyle : "fadeOut",
      animateIn: "fadeIn",
      animateOut: "fadeOut",
      lazyLoad : true,
      mouseDrag: false,
      touchDrag: false,
  });
}
function setCarouselGenres(element) {
  if ($(window).width() > 1200) {
      $(element).owlCarousel({
          items: 8,
          margin: 10,
          dots: false,
          loop: false,
          autoplay: false,
          nav: false,
          autoplayHoverPause: true,
          rtl: true,
          stagePadding: 55,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  } else if ($(window).width() < 1200 && $(window).width() > 900) {
      $(element).owlCarousel({
          items: 4,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: false,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  } else if ($(window).width() < 900) {
      $(element).owlCarousel({
          items: 2,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: false,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  }
}
let owl;
function setCarouselSpecialGenres(element) {
  if ($(window).width() > 1200) {
      owl = $(element).owlCarousel({
          items: 6,
          margin: 10,
          dots: false,
          loop: false,
          autoplay: false,
          nav: false,
          autoplayHoverPause: true,
          rtl: true,
          stagePadding: 55
      })
  } else if ($(window).width() < 1200 && $(window).width() > 900) {
      $(element).owlCarousel({
          items: 4,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: true,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
      })
  } else if ($(window).width() < 900) {
      $(element).owlCarousel({
          items: 2,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: true,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
      })
  }

  $(".with-detail .item").click(function(event) {
    event.preventDefault();
    if ($(window).width() < 800) {
      location.href = "movie.html";
    }
    else {
      $(".with-detail .item").each(function() {
        $(this).removeClass("slider-detail-active");
      });
      $(this).addClass("slider-detail-active");
      $("#itemDetail").slideDown(100);
    }
  });
}

$(function() {
  // setCarouselGenres("#genre1");
  // setCarouselGenres("#genre2");
  setCarouselSpecialGenres("#genre3");
  // setCarouselGenres("#genre4");
  getBanners();
  getItems();
});

function getBanners() {
  $.ajax({
    type: "GET",
    url: "/api/banner/list",
    success: function (data) {
      if(!data.banners) {
        $("#first-slider").addClass("first-mt-5");
        return;
      }
      for(let i = 0; i < data.banners.length; i++) {
        $("#banner").append(
          `
            <div class="item">
              <div class="banner-content photo-space">
                <div class="slider-wrap px-0">
                  <picture>
                    <source
                        media="(max-width: 768px)"
                        srcset="${data.banners[i].mobile_image}"
                        height="425px">
                    <source
                        media="(min-width: 768px)"
                        srcset="${data.banners[i].descktop_image}"
                        height="800px">
                    <img src="${data.banners[i].descktop_image}" 
                    alt="${data.banners[i].title}" height="800px">
                  </picture>
                  <div class="banner-shadow"></div>
                  <div class="banner-description">
                    <h2>${data.banners[i].title}</h2>
                    <span class="tags">
                      <span>${data.banners[i].description}</span>
                    </span>
                    <div class="d-flex justify-content-end mt-2 js-banner-btns-${data.banners[i].id}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `
        );
        if(data.banners[i].video && data.banners[i].video != "000000000000000000000000" && data.banners[i].pereview) {
          $(".js-banner-btns-"+data.banners[i].id).append(
            `
              <a role="button" onclick="startPreview('${data.banners[i].video}', 'main-page')" class="watch btn-secondary mr-2">پیش نمایش</a>
            `
          );
        }
        if(data.banners[i].link) {
          $(".js-banner-btns-"+data.banners[i].id).append(
            `
              <a href="${data.banners[i].link}" class="watch btn-primary">
                ${data.banners[i].serial ? 'قسمت ها' : 'پخش فیلم'}
              </a>
            `
          );
        }
      }
      setCarouselForBanner("#banner");
    },
    error: function (xhr, status, error) {
    }
  });
}

function getItems() {
  $.ajax({
    type: "GET",
    url: "/api/mainpage/list",
    success: function (result) {
      if (result.mainpages && result.mainpages.length) {
        appendMainItems(result.mainpages);
      }
    },
    error: function (xhr, status, error) {
    }
  });
}

function appendMainItems(items) {
  items.sort((a, b) => {
    return a.position - b.position;
  });
  for (let i = items.length - 1; i >= 0; i--) {
    if (items[i].image && items[i].video && items[i].video!= "000000000000000000000000") {
      getVideoByID(items[i].video, i - 1);
    }
    else {
      $("#genre-sliders").after(`
        <div class="genre-sliders-wrapper pos-${i} ${ i == 0 ? 'slider-overly' : ''}" id="${ i == 0 ? 'first-slider' : ''}">
          <div class="slider-content">
            <div class="mb-4 genre-sliders">
              <div class="content-title">
                <span class="titles">${items[i].title}</span>
              </div>
            </div>
            <div class="carousel-wrap" dir="ltr">
              <div class="owl-carousel" id="genre-${items[i].id}">
              </div>
            </div>
          </div>
        </div>
      `);
    }
  }
  for (let i = 0; i < items.length; i++) {
    if(!items[i].image) {
      getVideosByFilter(items[i]);
    }
  }
}

function getVideoByID(videoID, index) {
  $.ajax({
    type: "GET",
    url: "/api/video/load/" + videoID,
    success: function (result) {
      let neighbor = index == -1 ? '#genre-sliders' : ".pos-"+index;
      $(neighbor).after(`
        <div class="detail js-item-detail genre-sliders mb-5" style="position:relative;background-image: linear-gradient(to left, #0C151E, rgba(0,0,0,0)),url('/api/media/${result.video.desktop_banners[0]}')">
          <div class="detail-shadow"></div>
          <h2>${result.video.name}</h2>
          <div class="details">
            <span class="badge ml-4 ${result.video.age_range.id == '000000000000000000000000' ? 'd-none' : ''}">${result.video.age_range.name}</span>
            <span class="ml-4">${new Date(result.video.release_date).getFullYear()}</span>
            <span class="ml-4 ${result.video.imdb_rating ? '': 'd-none'}">امتیاز: ${parseFloat(result.video.imdb_rating).toFixed(1)}</span>
            <span class="like ${result.video.like ? '': 'd-none'}">
              <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.33801 8.43116L7.55247 13.3296L7.55247 13.3296C7.65088 13.422 7.70008 13.4683 7.75051 13.4973C7.90495 13.5862 8.09505 13.5862 8.24949 13.4973C8.29992 13.4683 8.34913 13.422 8.44753 13.3296L13.662 8.43116C15.1291 7.05294 15.3073 4.78495 14.0734 3.19455L13.8413 2.89551C12.3652 0.992935 9.40222 1.31201 8.365 3.48524C8.21849 3.79222 7.78151 3.79222 7.635 3.48524C6.59778 1.31201 3.63479 0.992934 2.15866 2.8955L1.92664 3.19455C0.692712 4.78495 0.870873 7.05294 2.33801 8.43116Z" fill="white" stroke="white" stroke-width="2"/>
              </svg>
              <span class="px-2">${result.video.like}</span>
            </span>
          </div>
          <div class="description py-2 py-lg-5">
            ${result.video.description}
          </div>
          <div class="my-4">
            <a role="button" onclick="startPreview('${result.video.preview}', 'item')" class="watch btn-secondary ${result.video.preview ? '' : 'd-none'}">پیش نمایش</a>
          </div>
          <div class="agents pb-5">
            <p class="${result.video.actors.length ? '' : 'd-none'}">ستارگان: ${splitter(result.video.actors, 'name')}</p>
            <p class="${result.video.director.length ? '' : 'd-none'}">کارگردان: ${splitter(result.video.director, 'name')}</p>
          </div>
        </div>
      `
      );
    },
    error: function (xhr, status, error) {
    }
  });
}

function getVideosByFilter(item) {
  let params = "";
  if (item.filter["sort"] && item.filter["limit"]) {
    params = "?sort=" + item.filter["sort"][0].id + "&limit=" + item.filter["limit"];
  }
  else if(item.filter["sort"]) {
    params = "?sort=" + item.filter["sort"][0].id;
  }
  else if (item.filter["limit"]) {
    params = "?limit=" + item.filter["limit"];
  }
  delete item.filter["sort"];
  delete item.filter["limit"];
  $.ajax({
    type: "POST",
    url: "/api/video/search/advanced" + params,
    data: item.filter,
    success: function (result) {
      for (let i = 0; i < result.videos.length; i++) {
        $("#genre-"+item.id).append(`
          <div class="item">
            <a class="slide-one" href="/movie/${result.videos[i].imdb}/${result.videos[i].key[0]}">
              <div class="slide-image">
                <img src="/media/${result.videos[i].mobile_banners[0]}" loading="lazy" alt="${result.videos[i].name}">
                <div class="info-overlay">
                  <div class="info-name pe-3">
                    <span>${result.videos[i].type == 2 ? 'فیلم' : 'سریال'}</span>
                    <span> - </span>
                    <span>
                      <div class="d-inline-block">
                        ${new Date(result.videos[i].release_date).getFullYear()}
                      </div>
                    </span>
                  </div>
                  <div class="info-name pe-3">
                    <div class="d-flex align-items-center ${result.videos[i].like ? '': 'd-none'}">
                      <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.33801 8.43116L7.55247 13.3296L7.55247 13.3296C7.65088 13.422 7.70008 13.4683 7.75051 13.4973C7.90495 13.5862 8.09505 13.5862 8.24949 13.4973C8.29992 13.4683 8.34913 13.422 8.44753 13.3296L13.662 8.43116C15.1291 7.05294 15.3073 4.78495 14.0734 3.19455L13.8413 2.89551C12.3652 0.992935 9.40222 1.31201 8.365 3.48524C8.21849 3.79222 7.78151 3.79222 7.635 3.48524C6.59778 1.31201 3.63479 0.992934 2.15866 2.8955L1.92664 3.19455C0.692712 4.78495 0.870873 7.05294 2.33801 8.43116Z" fill="white" stroke="white" stroke-width="2"></path>
                      </svg>
                      <span>${result.videos[i].like}</span>
                    </div>
                  </div>
                  <div class="info-name pe-3 ${result.videos[i].imdb_rating ? '': 'd-none'}">
                    <div class="d-flex align-items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" width="35" height="20" fill="#ffffff" class="t-icon-0-1-156"><path d="M11.107 9.587c.1-.424.122-.824.2-1.248l.122-.965.122-.918a7 7 0 0 1 .1-.706l.1-.753c.05-.235.073-.494.1-.73.024-.212 0-.212.22-.212h3.746c.122 0 .147.024.147.14V15.78c0 .094-.024.14-.122.14h-2.36c-.122 0-.147-.024-.147-.14v-6.3h-.024l-.27 1.46-.245 1.295-.22 1.295-.2 1.13-.22 1.177c-.024.094-.05.118-.147.118h-1.662c-.05 0-.1 0-.1-.07l-.343-1.837-.367-1.884-.367-1.978-.147-.73v6.357c0 .118-.024.165-.147.165h-2.4c-.122 0-.147-.024-.147-.14V4.242c0-.094.024-.14.122-.14h3.453c.073 0 .122 0 .147.094l.27 1.483.294 1.6.22 1.32a3.5 3.5 0 0 1 .272.99zm6.288.3V4.195c0-.14.05-.165.17-.165h3.942a4.46 4.46 0 0 1 1.665.235 2.66 2.66 0 0 1 1.175.8 2.25 2.25 0 0 1 .367.894 6.31 6.31 0 0 1 .049 1.176v6.352a1.8 1.8 0 0 1-.661 1.459 2.76 2.76 0 0 1-1.1.518 6.59 6.59 0 0 1-1.665.212l-3.8.024c-.122 0-.1-.07-.1-.14l-.043-5.67zm2.9-.094v3.858c0 .094.024.118.122.118a4.51 4.51 0 0 0 .563-.024.56.56 0 0 0 .514-.588V6.383c.006-.183-.1-.35-.27-.423a2.18 2.18 0 0 0-.784-.118c-.122 0-.147.024-.147.14zm8.917-3.08a5.12 5.12 0 0 1 1.494-.73 2.02 2.02 0 0 1 1.812.4 1.45 1.45 0 0 1 .588 1.2v7c-.022.595-.464 1.1-1.053 1.177a4.36 4.36 0 0 1-1.1.047 4.08 4.08 0 0 1-1.053-.26l-.857-.26c-.086-.026-.18.013-.22.094-.07.133-.12.276-.147.424-.024.07-.05.094-.122.094H26.12c-.073 0-.1-.024-.1-.07V4.22c0-.14.025-.165.17-.165h2.8c.17 0 .17 0 .17.188l.05 2.472zm.2 3.955v3.085a.46.46 0 0 0 .1.306c.082.133.244.2.392.14.17-.047.22-.118.22-.33v-6.1a.7.7 0 0 0-.024-.235c-.046-.186-.226-.308-.416-.282-.147.024-.27.07-.27.33zM1.924 9.963V4.195c0-.118.024-.14.147-.14h2.62c.122 0 .122.024.122.14v11.583c0 .118-.024.14-.147.14h-2.62c-.122 0-.147-.024-.147-.14l.025-5.815z"></path></svg>
                      <span>${parseFloat(result.videos[i].imdb_rating).toFixed(1)}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="slide-content">
                <div class="d-flex justify-content-end">
                  <h2>${result.videos[i].name}</h2>
                </div>
              </div>
            </a>
          </div>
        `);
      }
      setCarouselGenres("#genre-"+item.id);
    },
    error: function (xhr, status, error) {
    }
  });
}