function setCarouselForBanner(element) {
  $(element).owlCarousel({
      autoplay: true,
      autoplayHoverPause: true,
      rtl: false,
      autoplayTimeout: 8000,
      autoplaySpeed: 1500,
      stagePadding: 0,
      items: 1,
      loop:true,
      margin:0,
      singleItem:true,
      nav:true,
      navText: [
          "<img src='../static/img/index/left_double.svg' alt='left icon'>",
          "<img src='../static/img/index/right_double.svg' alt='right icon'>"
      ],
      dots:true,
      transitionStyle : "fadeOut",
      animateIn: "fadeIn",
      animateOut: "fadeOut",
      lazyLoad : true,
      mouseDrag: false,
      touchDrag: false,
  });
}
function setCarouselGenres(element) {
  if ($(window).width() > 1200) {
      $(element).owlCarousel({
          items: 8,
          margin: 10,
          dots: false,
          loop: false,
          autoplay: false,
          nav: false,
          autoplayHoverPause: true,
          rtl: true,
          stagePadding: 55,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  } else if ($(window).width() < 1200 && $(window).width() > 900) {
      $(element).owlCarousel({
          items: 4,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: false,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  } else if ($(window).width() < 900) {
      $(element).owlCarousel({
          items: 2,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: false,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
          responsive:{
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items:6
            }
          }
      })
  }
}
let owl;
function setCarouselSpecialGenres(element) {
  if ($(window).width() > 1200) {
      owl = $(element).owlCarousel({
          items: 6,
          margin: 10,
          dots: false,
          loop: false,
          autoplay: false,
          nav: false,
          autoplayHoverPause: true,
          rtl: true,
          stagePadding: 55
      })
  } else if ($(window).width() < 1200 && $(window).width() > 900) {
      $(element).owlCarousel({
          items: 4,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: true,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
      })
  } else if ($(window).width() < 900) {
      $(element).owlCarousel({
          items: 2,
          margin: 25,
          dots: false,
          loop: false,
          autoplay: true,
          nav: true,
          navText: ["<i class='fas fa-chevron-left nav-btn prev-slide'></i>", "<i class='fas fa-chevron-right nav-btn next-slide'></i>"],
          autoplayHoverPause: true,
          rtl: true,
      })
  }

  $(".with-detail .item").click(function(event) {
    event.preventDefault();
    if ($(window).width() < 800) {
      location.href = "movie.html";
    }
    else {
      $(".with-detail .item").each(function() {
        $(this).removeClass("slider-detail-active");
      });
      $(this).addClass("slider-detail-active");
      $("#itemDetail").slideDown(100);
    }
  });
}

var videoID = "";
var videoKey = "";
let selectedVideoUrl;
let selectedVideoPoster;
let allVideoUrls;
$(function() {
  moment.loadPersian(
    {
      dialect: 'persian-modern',
      usePersianDigits: true
    },
  );
  videoKey = location.pathname.split("/").pop();
  if (videoKey == 'movie') {
    getVideo('byID');
  }
  else {
    getVideo('byKey');
  }
  $(".js-apply-comment").on("click", function() {
    sendComment();
  });
});

function getVideo(type) {
  let url = '/api/video/load/key/'+ videoKey;
  if (type == 'byID') {
    url = '/api/video/load/' + localStorage.getItem("fb-serial");
  }
  else {
    url = '/api/video/load/key/'+ videoKey;
  }
  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
      let videoByKey = type == 'byID' ? data.video : data.actor;
      videoID = videoByKey.id;
      // set meta tags , title
      document.title = videoByKey.google_title;
      $("head").prepend(`<meta name="description" content="${videoByKey.google_description}">
        <meta name="keywords" content="${videoByKey.google_tags}">
        <meta property="og:title" content="${videoByKey.google_title}">
        <meta property="og:url" content="${location.href}">
        <meta property="og:description" content="${videoByKey.google_description}">
        <meta property="og:image" content="/api/media/${videoByKey.tump_icon}">
        <link rel="canonical" href="${location.href}">
      `);
      // end
      if(videoByKey.description) {
        $(".js-video-name-2").html("درباره فیلم " + videoByKey.name);
        $(".js-video-description-2").html(videoByKey.description);
      }
      for(let i = 0; i < videoByKey.desktop_banners.length; i++) {
        $(".js-video-banner").prepend(
          `
          <div class="item">
            <div class="banner-content photo-space">
              <div class="slider-wrap px-0">
                <picture>
                  <source
                    media="(max-width: 768px)"
                    srcset="/api/media/${videoByKey.mobile_banners[i] ? videoByKey.mobile_banners[i] : videoByKey.desktop_banners[i]}"
                    height="425px">
                  <source
                    media="(min-width: 768px)"
                    srcset="/api/media/${videoByKey.desktop_banners[i]}"
                    height="800px">
                  <img src="/api/media/${videoByKey.desktop_banners[i]}" 
                    alt="${videoByKey.name} ${i}" height="800px">
                  </picture>
                  <div class="banner-shadow"></div>
                  <div class="banner-description">
                    <h2>${videoByKey.name}</h2>
                    <span class="tags">
                      <span></span>
                    </span>
                    <div class="details my-3">
                      <span class="ml-4">${convertToFaDigit(new Date(videoByKey.release_date).getFullYear())}</span>
                      <span class="ml-4" dir="rtl"><span>${convertToFaDigit(Math.round(videoByKey.length / 60))}</span> <span> دقیقه </span></span>
                      <span class="ml-4 ${videoByKey.imdb_rating ? '': 'd-none'}">امتیاز: ${convertToFaDigit(parseFloat(videoByKey.imdb_rating).toFixed(1))}</span>
                      <span class="like ml-4 ${videoByKey.like ? '': 'd-none'}">
                        <span class="px-2">${videoByKey.like}</span>
                        <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M2.33801 8.43116L7.55247 13.3296L7.55247 13.3296C7.65088 13.422 7.70008 13.4683 7.75051 13.4973C7.90495 13.5862 8.09505 13.5862 8.24949 13.4973C8.29992 13.4683 8.34913 13.422 8.44753 13.3296L13.662 8.43116C15.1291 7.05294 15.3073 4.78495 14.0734 3.19455L13.8413 2.89551C12.3652 0.992935 9.40222 1.31201 8.365 3.48524C8.21849 3.79222 7.78151 3.79222 7.635 3.48524C6.59778 1.31201 3.63479 0.992934 2.15866 2.8955L1.92664 3.19455C0.692712 4.78495 0.870873 7.05294 2.33801 8.43116Z" fill="white" stroke="white" stroke-width="2"/>
                        </svg>
                      </span>
                      <span class="${videoByKey.genres.length ? '' : 'd-none'}">ژانر: ${splitter(videoByKey.genres, 'name', '، ')}</span>
                    </div>
                    <div class="summary">
                      ${videoByKey.description.slice(0, 180) + "..."}
                    </div>
                    <div class="d-flex justify-content-end mt-2 js-video-btns">
                    </div>
                    <div class="agents">
                      <p class="${videoByKey.actors.length ? '' : 'd-none'}">${splitter(videoByKey.actors, 'name', '، ')} :ستارگان</p>
                      <p class="${videoByKey.director.length ? '' : 'd-none'}">${splitter(videoByKey.director, 'name', '، ')} :کارگردان</p>
                    </div>
                  </div>
                  <div class="col-xl-7 col-lg-8 col-md-10 col-12 movie-shots d-none">
                    <div class="shots-wrapper js-video-shots">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          `
        );
      }
      setCarouselForBanner("#banner");
      if(videoByKey.preview) {
        $(".js-video-btns").append(
          `
            <a role="button" onclick="startPreview('${videoByKey.preview}', 'movie-page')" class="watch btn-secondary mr-2">پیش نمایش</a>
          `
        );
      }
      if(videoByKey.video_urls.length) {
        for (let i = 0; i < videoByKey.video_urls.length; i++) {
          if(i == 0) {
            selectedVideoUrl = videoByKey.video_urls[i];
            allVideoUrls = videoByKey.video_urls;
            selectedVideoPoster = videoByKey.desktop_banners[i];
            $(".js-video-btns").append(
              `
                <a role="button" onclick="startVideo()" class="watch btn-primary ml-2">پخش فیلم</a>
              `
            );
          }
        }
      }
      if(videoByKey.images.length) {
        $(".movie-shots").removeClass("d-none");
        for(let i = 0; i < videoByKey.images.length; i++) {
          // if(i < 2) {
            $(".js-video-shots").append(
              `
                <img src="/api/media/${videoByKey.images[i]}" width="360px" height="200px" alt="shots-${i}">
              `
            );
          // }
        }
      }
      if(videoByKey.similar_videos.length) {
        getVideosByIds(videoByKey.similar_videos);
      }
      else if (!videoByKey.similar_videos.length) {
        $(".js-similar-vid").addClass("d-none");
      }
      if(videoByKey.actors.length) {
        for (let i = 0; i < videoByKey.actors.length; i++) {
          $("#genre3").append(`
            <div class="item">
              <a class="slide-one actor-wrapper" href="/actor/${videoByKey.actors[i].key}">
                <div class="slide-image actor-image">
                  <img src="${videoByKey.actors[i].profile ? '/api/media/'+ videoByKey.actors[i].profile : '/static/img/movie/user-default.png'}" loading="lazy" alt="${videoByKey.actors[i].name}">
                </div>
                <div class="slide-content">
                  <div class="d-flex justify-content-center">
                    <h2 class="text-center">${videoByKey.actors[i].name}</h2>
                  </div>
                </div>
              </a>
            </div>
          `);
        }
        setCarouselGenres("#genre3");
      }
      if(videoByKey.director.length) {
        for (let i = 0; i < videoByKey.director.length; i++) {
          $("#genre4").append(`
            <div class="item">
              <a class="slide-one actor-wrapper" href="/director/${videoByKey.director[i].key}">
                <div class="slide-image actor-image">
                  <img src="${videoByKey.director[i].profile ? '/api/media/'+ videoByKey.director[i].profile : '/static/img/movie/user-default.png'}" loading="lazy" alt="${videoByKey.director[i].name}">
                </div>
                <div class="slide-content">
                  <div class="d-flex justify-content-center">
                    <h2 class="text-center">${videoByKey.director[i].name}</h2>
                  </div>
                </div>
              </a>
            </div>
          `);
        }
        setCarouselGenres("#genre4");
      }
      if(videoByKey.age_range) {
        $(".js-movie-data").append(`
          <span class="badge ${videoByKey.age_range.id == '000000000000000000000000' ? 'd-none' : ''}">
            ${videoByKey.age_range.name}
          </span>
          <span class="ml-4 ${videoByKey.age_range.id == '000000000000000000000000' ? 'd-none' : ''}">
            ${videoByKey.age_range.description}
          </span>
        `);
      }
      if(videoByKey.country.length) {
        $(".js-movie-data").append(`
          <span class="ml-4">
            کشور:‌ ${videoByKey.country}
          </span>
        `);
      }
      if(videoByKey.language.length) {
        $(".js-movie-data").append(`
          <span class="ml-4">
            زبان:‌ ${videoByKey.language}
          </span>
        `);
      }
      if(videoByKey.tags.length) {
        for (let i = 0; i < videoByKey.tags.length; i++) {
          $(".js-movie-data").append(`
            <div class="item-tag">
              ${videoByKey.tags[i].name}
            </div>
          `);
        }
      }
      if(videoByKey.free) {
        $(".js-movie-download").append(`
          <h3 class="titles">
           لینک های دانلود
          </h3>
        `);
        for(let i = 0; i < videoByKey.video_urls.length; i++) {
          $(".js-movie-download").append(`
            <a class="item-download item-dn-${videoByKey.video_urls[i].quality}" href="${videoByKey.video_urls[i].url}">
              <img src='/static/img/common/download.svg' class="mx-1" width="15px" loading="lazy" alt="download">
              ${videoByKey.video_urls[i].quality}
            </a>
          `);
          if(videoByKey.video_urls[i].subtitles) {
            for(let j = 0; j < videoByKey.video_urls[i].subtitles.length; j++) {
              $(".item-dn-"+videoByKey.video_urls[i].quality).after(`
                <a class="sub-link" href="${videoByKey.video_urls[i].subtitles[j].url}" title="${videoByKey.video_urls[i].subtitles[j].language}">
                  دانلود زیرنویس ${videoByKey.video_urls[i].subtitles[j].language}
                </a>
              `);
            }
          }
        }
      }
      if (videoByKey.data) {
        getRelatedData(videoByKey.data);
      }
      getComments();
    },
    error: function (xhr, status, error) {
      location.replace("/");
    }
  });
}

function getRelatedData(dataID) {
  $.ajax({
    type: "GET",
    url: '/api/category/data/load/'+ dataID,
    dataType: 'json',
    contentType: 'application/json',
    success: function (result) {
      if (result.data.season && result.data.season.length) {
        $("#episodes").removeClass("d-none");
        for(let i = 0; i < result.data.season.length; i++) {
          $(".js-season").append(`
            <div class="d-flex align-items-center">
              <div class="t-seasons-0">
                <div class="t-season-1">فصل ${convertToFaDigit(i+1)}</div>
              </div>
              <div class="t-seasonInfo-2">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" class="t-icon-0-1-268">
                  <g clip-path="url(#clip0_16448:5568)"><path fill-rule="evenodd" clip-rule="evenodd" d="M24.19 6H5C3.34 6 2 7.35 2 9V18.79C2 20.44 3.34 21.79 5 21.79H24.19C25.85 21.79 27.19 20.44 27.19 18.79V9C27.19 7.35 25.85 6 24.19 6ZM17.11 14.67L14.24 16.33C13.57 16.71 12.74 16.23 12.74 15.46V12.15C12.74 11.38 13.57 10.9 14.24 11.28L17.11 12.94C17.78 13.32 17.78 14.29 17.11 14.67Z" fill="white"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M6.36001 24.0699H22.74C22.99 24.0699 23.19 23.8699 23.19 23.6199C23.19 23.3699 22.99 23.1699 22.74 23.1699H6.36001C6.11001 23.1699 5.91001 23.3699 5.91001 23.6199C5.90001 23.8699 6.11001 24.0699 6.36001 24.0699Z" fill="white"></path></g><defs><clipPath id="clip0_16448:5568"><rect width="25.19" height="18.07" fill="white" transform="translate(2 6)"></rect></clipPath></defs>
                </svg>
                <div class="d-inline-block s-1">
                  <span class="t-seasonLength-3 ${result.data.season.length}">تعداد کل فصل&zwnj;ها: ${convertToFaDigit(result.data.season.length)}</span>
                  <span class="t-seprator-4">|</span><span>${result.data.season[i].description}</span>
                </div>
              </div>
            </div>
            <div>
              <div class="margin-bottom">
                <div class="row js-episodes-${i}">
  
                </div>
              </div>
            </div>
          `);
          getSeasonVideos(result.data.season[i].videos, i);
        }
      }
    }
  });
}

function getSeasonVideos(videoIDs, idx) {
  let body = {id: videoIDs};
  $.ajax({
    type: "POST",
    url: "/api/video/load/ids",
    contentType: 'application/json',
    data: JSON.stringify(body),
    success: function (result) {
      for (let i = 0; i < result.data.length; i++) {
        $(".js-episodes-"+idx).append(`
          <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="t-root-5">
              <div class="clearfix">
                <a class="d-block" href="/movie/${result.data[i].key}">
                  <div class="t-imageWrapper-7">
                    <img src="/api/media/${result.data[i].mobile_banners[0]}" width="300px" height="100%" alt="${result.data[i].name}" title="${result.data[i].name}" class="rounded t-image-8">
                    <div class="t-placeholder-9"></div>
                    <div class="t-gradient-10"></div>
                    <span class="t-play-11">
                      <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" class="t-playIcon-12">
                        <path d="M14.66 8.347l-8.537-4.93c-.502-.287-1.12-.286-1.62.005s-.81.825-.81 1.404v9.858c-.001.58.307 1.115.808 1.406s1.12.293 1.62.005l8.537-4.93a1.64 1.64 0 0 0 0-2.82z"></path>
                      </svg>
                    </span>
                    <div class="t-time-6">${convertToFaDigit(Math.round(result.data[i].length / 60))}دقیقه</div>
                  </div>
                </a>
                <div class="mt-2 clearfix">
                  <span class="d-block">
                    <h2 class="t-s-title">${result.data[i].name}</h2>
                  </span>
                </div>
              </div>
              <div class="t-s-description-13 text-justify">
                ${result.data[i].description.slice(0, 180) + "..."}
              </div>
            </div>
          </div>
        `);
      }
    },
    error: function (xhr, status, error) {
    }
  });

}

function getVideosByIds(similar_videos) {
  let body = {id: similar_videos};
  $.ajax({
    type: "POST",
    url: "/api/video/load/ids",
    contentType: 'application/json',
    data: JSON.stringify(body),
    success: function (result) {
      for (let i = 0; i < result.data.length; i++) {
        $("#genre2").append(`
          <div class="item">
            <a class="slide-one" href="/movie/${data.videos[i].key}">
              <div class="slide-image">
                <img src="/api/media/${result.data[i].mobile_banners[0]}" loading="lazy" alt="${result.data[i].name}">
              </div>
              <div class="slide-content">
                <div class="d-flex justify-content-end">
                  <h2>${result.data[i].name}</h2>
                </div>
              </div>
            </a>
          </div>
        `);
      }
      setCarouselGenres("#genre2");
    },
    error: function (xhr, status, error) {
    }
  });
}

$(".js-input-comment").on('input', function() {
  if ($(".js-input-comment").val().length != 0) {
    $(".js-feedback").html(" ");
    return;
  }
});

function sendComment() {
  if ($(".js-input-comment").val().length == 0) {
    $(".js-feedback").html("لطفا نظر خود را بنویسید");
    return;
  }
  let body = {
    description: $(".js-input-comment").val(),
    parrent: ""
  }
  $.ajax({
    type: "POST",
    url: '/api/comment/add/'+videoID,
    data: JSON.stringify(body),
    headers: {
      "Authorization": localStorage.getItem("current-token")
    },
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
      // toDo: msg
      $(".js-input-comment").val("");
      getComments();
    },
    error: function (xhr, status, error) {
      if(xhr && xhr.status == 401) {
        $(".js-feedback").html("ابتدا وارد حساب کاربری خود شوید");
      }
    }
  });
}

function getComments() {
  $.ajax({
    type: "GET",
    url: '/api/comment/list/'+videoID,
    dataType: 'json',
    success: function (data) {
      // toDo: msg
      $(".js-video-comments").html(" ");
      if(data.comments.length) {
        for(let i = 0 ; i < data.comments.length; i++) {
          $(".js-video-comments").append(
            `
            <div class="comments-list-title">
              <img src="../static/img/movie/user-default.png" alt="avatar ${i}">
              <span class="title mr-5">
                <span>${data.comments[i].created_by}</span> در <span>${moment(data.comments[i].created_at).format("ddd jD jMMMM")}</span> گفته است
              </span>
            </div>
            <div class="comments-list-body mb-4">
              ${data.comments[i].description}
            </div>
            <hr>
            `
          );
        }
      }
    },
    error: function (xhr, status, error) {
      // msg
    }
  });
}

let videoElement = "";
let onlyVTT = true;
function startVideo() {
  $("#filmModal").slideToggle(100);
  $('.js-questions1').empty();
  videoElement = `
    <video
      id="v_player"
      class="video-js vjs-tech w-100"
      controls
      preload="auto"
      width="640"
      height="90vh"
      autoplay
      playsinline
      tabindex="-1"
      poster="/api/media/${selectedVideoPoster}"
      data-setup="{}"
    >
  `;
  for (let i = 0; i < allVideoUrls.length; i++) {
    videoElement += `
      <source src="${allVideoUrls[i].url}" type="video/mp4" label="${allVideoUrls[i].quality}" res="${allVideoUrls[i].quality}" />
    `;
  }
  for (let i = 0; i < allVideoUrls.length; i++) {
    for (let j = 0; j < allVideoUrls[i].subtitles.length; j++) {
      if (ext(allVideoUrls[i].subtitles[j].url, allVideoUrls[i].subtitles[j].url.lastIndexOf(".")) == 'srt') {
        onlyVTT = false;
        convertTOVTT(allVideoUrls[i].subtitles[j].url, allVideoUrls[i].subtitles[j].language);
      }
      else {
        videoElement += `
          <track src="${allVideoUrls[i].subtitles[j].url}" kind="subtitles" srclang="${allVideoUrls[i].subtitles[j].language}" label="${allVideoUrls[i].subtitles[j].language}">
        `;
      }
    }
  }
  if (onlyVTT) {
    videoElement += `
      <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a
          web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a
          >
        </p>
      </video>
    `;
    $('.js-questions1').append(`
      <div class="col-lg-12 d-flex justify-content-end">
        <span class="cursor-pointer" title="بستن" onclick="closeModalVideo()">
          <img src="../static/img/common/cross.svg" alt="cross">
        </span>
      </div>
      <div class="col-lg-12">
        <div>
          ${videoElement}
        </div>
      </div>
    `);
    initApp();
    return;
  }
}

// video player
function initApp() {
  var player = videojs('v_player',
  {
    techOrder:['chromecast','html5'],
  });

  player.viavi({
    shareMenu: false,
    video_id: 'movie124',
    resume: true,
    contextMenu: false,
    buttonRewind: true,
    buttonForward: true,
    mousedisplay:true,
    textTrackSettings: false,
    theaterButton: false  	
  });
  player.ready(function() {
    this.hotkeys({
      volumeStep: 0.1,
      seekStep: 5,
      alwaysCaptureHotkeys: true
    });
  });
}

function convertTOVTT(subURL, subLang) {
  let body = { url: subURL };
  $.ajax({
    type: "POST",
    url: "/api/video/convert",
    data: body,
    success: function (data) {
      videoElement += `
        <track src="/api/media/${data.file.path}" kind="subtitles" srclang="${subLang}" label="${subLang}">
      `;
      videoElement += `
      <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a
          web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a
          >
        </p>
      </video>
    `;
    $('.js-questions1').append(`
      <div class="col-lg-12 d-flex justify-content-end">
        <span class="cursor-pointer" title="بستن" onclick="closeModalVideo()">
          <img src="../static/img/common/cross.svg" alt="cross">
        </span>
      </div>
      <div class="col-lg-12">
        <div>
          ${videoElement}
        </div>
      </div>
    `);
    initApp();
    return;
    },
    error: function (xhr, status, error) {
    }
  });
}