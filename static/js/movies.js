var catID = "";
$(function() {
  catID = localStorage.getItem("fb-category");
  getVideosByCat();
});

function getVideosByCat() {
  if (!catID) {
    location.replace("/category");
  }
  let body = {category: catID};
  $.ajax({
    type: "POST",
    url: "/api/video/search/advanced",
    data: body,
    success: function (data) {
      for(let i = 0; i < data.videos.length; i++) {
        $(".lists-body").append(
          `
            <div class="col-xl-2 col-md-3 col-6 mb-4">
              <div class="item">
                <a class="slide-one" href="/movie/${data.videos[i].imdb}/${data.videos[i].key[0]}">
                  <div class="slide-image">
                    <img src="/api/media/${data.videos[i].mobile_banners[0]}" loading="lazy" alt="${data.videos[i].name}">
                    <div class="d-flex justify-content-start align-items-center px-2">
                      <h4> ${data.videos[i].name} </h4>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          `
        );
      }
    },
    error: function (xhr, status, error) {
    }
  });
}