var validator = "";
function check() {
  var $form = $("#check-form");
  $.validator.addMethod(
    "email_phone",
    function (value) {
      var pattern = new RegExp(
        "^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})$",
        "i"
      );
      const ePattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/, 'i');
      if (pattern.test(convertToEnDigit(value))) {
        return true;
      }
      else if (ePattern.test(convertToEnDigit(value))) {
        return true;
      }
      else {
        return false;
      }
    },
    "شماره موبایل یا ایمیل نامعتبر است"
  );
  validator = $form.validate({
    ignore: ".ignore",
    rules: {
      input: {
        required: true,
        email_phone: true,
        maxlength: 100
      },
      otp: {
        required: true,
        digits: true,
        maxlength: 4,
        minlength: 4
      }
    },
    messages: {
      input: {
          'required': 'شماره موبایل یا ایمیل را وارد نمایید',
          'email_phone': 'شماره موبایل یا ایمیل نامعتبر است',
          'maxlength': 'شماره موبایل یا ایمیل طولانی است'
      },
      otp: {
        'required': 'رمز یکبار مصرف را وارد نمایید',
        'digits': 'لطفا فقط عدد انگلیسی وارد نمایید',
        'maxlength': 'حداکثر ۴ کارکتر وارد نمایید',
        'minlength': 'حداقل ۴ کارکتر وارد نمایید'
      }
    },
    submitHandler: function () {
      $.ajax({
        type: "GET",
        url: `/api/user/check/otp?input=${$("#check-input").val()}&otp=${$("#check-code").val()}`,
        beforeSend: function () {
          showLoader();
        },
        success: function () {
          hideLoader();
          if(getParameterByName('page') == 'register') {
            window.location.href = "register2.html?input=" + $("#check-input").val();
          }
          else if (getParameterByName('page') == 'reset') {
            window.location.href = "new-password.html?input=" + $("#check-input").val();
          }
        },
        error: function (xhr, status, error) {
          hideLoader();
          if(xhr && xhr.responseJSON) {
            if(xhr.responseJSON["code"] == 4002) {
              $(".js-error").html("اطلاعات وارد شده صحیح نیست.");
            }
            else if(xhr.responseJSON["code"] == 400034 && xhr.responseJSON["error"] == "please try after 10 min") {
              $(".js-error").html("کد ارسال شده است،لطفا بعد از چند دقیقه دوباره امتحان کنید.");
            }
            else if(xhr.responseJSON["code"] == 400033) {
              $(".js-error").html("شماره موبایل یا ایمیل مورد نظر تایید نشده است.");
            }
            else if(xhr.responseJSON["code"] == 400030 && xhr.responseJSON["debug_msg"] == "password otp count is greather than 3") {
              $(".js-error").html("تعداد درخواست رمزیکبار مصرف بیش از حد مجاز می باشد،لطفا بعدا مجدد تلاش نمایید.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if(xhr.responseJSON["code"] == 400030) {
              $(".js-error").html("زمان استفاده از رمز یکبار مصرف به پایان رسیده است.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if(xhr.responseJSON["code"] == 400031) {
              $(".js-error").html("رمز یکبار مصرف وارد شده، اشتباه است.");
              $(".js-resend-code").removeClass("d-none");
            }
            else if (xhr.responseJSON["code"] == 4004 && xhr.responseJSON["error"] == "not found") {
              $(".js-error").html("کاربر یافت نشد.");
            }
            else {
              $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
            }
          }
          else {
            $(".js-error").html("مشکلی پیش امد لطفا مجددا تلاش نمایید.");
          }
          setTimeout(function() {
            $(".js-error").html("");
          }, 7000);
        }
      });
      return false;
    },
  });

}

var intervalKey;
$(document).ready(function() {
  check();
  if(getParameterByName('input') == "") {
    return location.replace('reset.html');
  }
  $(".js-user-info").html(getParameterByName('input'));
  $("#check-input").val(getParameterByName('input'));
  initCounter();
});

// Init Counter
let initCounter = function () {
  var $counter = $('.js-timer');

  $counter.countdown('destroy');
  var seconds = 120,
      now;

  now = new Date();
  now.setSeconds(now.getSeconds() + seconds);

  if (!now) return;
  $counter.countdown({
      date: now,
      hoursOnly: true,
      rtlTemplate: '%i:%s',
      template: '%i:%s',
      leadingZero: true,
      onComplete: function () {
        $(".js-resend-code").removeClass("d-none");
      }
  });
};

$("#check-form").on("submit", function() {
  $(".ignore").each(function() {
    $(this).removeClass("ignore");
  });
});
